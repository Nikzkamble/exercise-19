﻿using System;

namespace week_3_exercise_18
{
    class Program
    {
        static void Main(string[] args)
        {
            ///Start the program with Clear();
            Console.Clear();


            Console.WriteLine($"The answer of (6+7) * (3-2) = {(6 + 7) * (3 - 2)}");
            Console.WriteLine($"the answer of (6 * 7) + (3 * 2)  = {(6 * 7) + (3 * 2)}");
            Console.WriteLine($"the answer of (6 * 7) + 3 * 2  = {(6 * 7) + 3 * 2}");
            Console.WriteLine($"the answer of (3 * 2) + 6 * 7  = {(3 * 2) + 6 * 7}");
            Console.WriteLine($"the answer of (3 * 2) + 7 * 6 / 2  = {(3 * 2) + 7 * 6 / 2}");
            Console.WriteLine($"the answer of  6 + 7 * 3 - 2 = x   {6 + 7 * 3 - 2}");
            Console.WriteLine($"the answer of 3 * 2 + (3 * 2) =    {3 * 2 + (3 * 2)}");
            Console.WriteLine($"the answer of (6 * 7) * 7 + 6 =   {(6 * 7) * (7 + 6)}");
            Console.WriteLine($"the answer of (2 * 2) + 2 * 2 =    {(2 * 2) + 2 * 2}");
            Console.WriteLine($"the answer of 3 * 3 + (3 * 3) =   {3 * 3 + (3 * 3)}");
            Console.WriteLine($"the answer of (6\xB2 + 7) * 3 + 2 =  {(Math.Pow(6, 2) + 7) * 3 + 2}");
            Console.WriteLine($"the answer of (3 * 2) + 3\xB2 * 2 = x is = {(3*2)+Math.Pow(3,2)*2}"); 
            Console.WriteLine($"the answer of (6 * (7 + 7)) / 6 = x is = {(6*(7+7))/6}"); 
             Console.WriteLine($"the answer of ((2 + 2) + (2 * 2)) is = {((2 + 2) + (2 * 2))}");
             Console.WriteLine($"the answer of 4 * 4 + (3\xB2 * 3\xB2) is = {4 * 4 + (Math.Pow(3,2) * Math.Pow(3,2))}");




            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
